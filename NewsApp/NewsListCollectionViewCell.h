//
//  NewsListCollectionViewCell.h
//  NewsApp
//
//  Created by Admin on 02.09.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsListCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
