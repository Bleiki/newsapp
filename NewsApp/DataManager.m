//
//  DataManager.m
//  NewsApp
//
//  Created by Admin on 03.09.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

#pragma mark - Core Data stack
@synthesize persistentContainer = _persistentContainer;

+ (DataManager*) sharedManager {
    
    static DataManager* manager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}

- (NSPersistentContainer *)persistentContainer {
    
    @synchronized (self) {
        
        if (_persistentContainer == nil) {
            
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"NewsApp"];
            
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support
- (void)saveContext {
    
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    
    if ([context hasChanges]) {
        [context save:&error];
    }
}

#pragma mark - SuppertMethods
//For correct save.
- (NSManagedObject*)haveObjectWithEntityName:(NSString*) entityName predicateDate:(NSDate*) predicateDate {
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"pubDate == %@", predicateDate];
    
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    request.predicate = predicate;
    
    NSArray* objects = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    if (objects.count >= 1) {
        return objects.firstObject;
    } else {
        return nil;
    }
}

//Clear old news.
- (void)deleteOldNews {
    
    NSDate* date = [self getCheckDate];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"pubDate < %@", date];
    
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"News"];
    request.predicate = predicate;
    
    NSArray* objects = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject* object in objects) {
        [self.persistentContainer.viewContext deleteObject:object];
    }
    
    [self saveContext];
}

-(NSDate*) getCheckDate {
    
    NSDate* currentDate = [NSDate date];
    
    NSCalendar* calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:currentDate];
    
    dateComponents.hour   = 0;
    dateComponents.minute = 0;
    
    NSDate* date = [calendar dateFromComponents:dateComponents];
    
    return date;
}

@end
