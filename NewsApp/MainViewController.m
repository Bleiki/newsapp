//
//  MainViewController.m
//  NewsApp
//
//  Created by Admin on 02.09.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import "MainViewController.h"
#import "News+CoreDataClass.h"
#import "DataManager.h"
#import "NewsListCollectionViewCell.h"
#import "NewsContentCollectionViewCell.h"

@interface MainViewController ()

@property (strong, nonatomic) NewsListCollectionViewCell* newsListCell;
@property (strong, nonatomic) NewsContentCollectionViewCell* newsContentCell;
@property (strong, nonatomic) NSIndexPath* selectedCell; //For get context from webView.

@end

@implementation MainViewController

@synthesize fetchedResultsController = _fetchedResultsController;

//For parsing.
static NSString* keyForItem    = @"item";
static NSString* keyForLink    = @"link";
static NSString* keyForTitle   = @"title";
static NSString* keyForPubDate = @"pubDate";

//For collection view.
static NSInteger numberOfViews = 2;
static NSInteger rowOfNewsList = 0;

//For load content.
BOOL getLocalContent;

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Get news.
    [self getRSSData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return numberOfViews;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    NSString* identifier = indexPath.row == rowOfNewsList ? @"NewsListCell" : @"NewsContentCell";
    
    if (indexPath.row == rowOfNewsList) {
        
        NewsListCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        self.newsListCell = cell;
        
        return cell;

    } else {
        
        NewsContentCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.headerLabel.text = @"Выберите новость";
        
        self.newsContentCell = cell;
        
        return cell;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Set size for CollectionViewCells 30:70.
    if (indexPath.row == rowOfNewsList) {
        return CGSizeMake(self.view.bounds.size.width * 0.3, self.view.bounds.size.height);
    } else {
        return CGSizeMake(self.view.bounds.size.width * 0.7, self.view.bounds.size.height);
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.news.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* identifier = @"NewsCell";
    
    News* news = [self.news objectAtIndex:indexPath.row];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd.MM.yyyy HH:mm";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    cell.textLabel.text       = news.title;
    cell.detailTextLabel.text = [dateFormatter stringFromDate:news.pubDate];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedCell = indexPath;
    
    News* news = [self.news objectAtIndex:indexPath.row];
    
    self.newsContentCell.headerLabel.text = news.title;
    
    if (getLocalContent == NO) {
        
        //If haven't context, then get him.
        NSString* urlString = news.link;
        NSArray* substrings = [urlString componentsSeparatedByString:@"\n"]; //Get correct string.
        
        NSURL* url = [[NSURL alloc] initWithString:substrings.firstObject];
        
        NSURLRequest* request = [NSURLRequest requestWithURL:url];
        
        if (self.newsContentCell.webView.loading) {
            [self.newsContentCell.webView stopLoading];
        }
        
        [self.newsContentCell.webView loadRequest:request];
        
    } else {
        
        //If have context, then load him.
        [self.newsContentCell.webView loadHTMLString:news.content baseURL:nil];
    }

}

#pragma mark - NSXMLParserDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    
    self.currentElement = elementName;
    
    if ([elementName isEqualToString:keyForItem]) {
        
        self.currentLink    = [NSMutableString string];
        self.currentTitle   = [NSMutableString string];
        self.currentPubDate = [NSMutableString string];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([self.currentElement isEqualToString:keyForTitle]) {
        
        [self.currentTitle appendString:string];
        
    } else if ([self.currentElement isEqualToString:keyForPubDate]) {
        
        [self.currentPubDate appendString:string];
        
    } else if ([self.currentElement isEqualToString:keyForLink]) {
        
        [self.currentLink appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:keyForItem]) {
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        dateFormatter.locale     = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        dateFormatter.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss ZZZ";
        
        NSArray* dateStrings = [self.currentPubDate componentsSeparatedByString:@"\n"]; //Get correct string.
        
        NSString* dateString = [dateStrings firstObject];
        
        NSDate* date = [dateFormatter dateFromString:dateString];
        
        NSDate* checkDate = [DataManager.sharedManager getCheckDate];
        
        if (checkDate < date) {
            
            NSManagedObject* checkObject = [DataManager.sharedManager haveObjectWithEntityName:@"News" predicateDate:date];
            
            if (checkObject) {
                
                //If have news at this date.
                [self.news addObject:checkObject];
                
            } else {
                
                //If haven't news at this date.
                News* news = [NSEntityDescription insertNewObjectForEntityForName:@"News" inManagedObjectContext: [DataManager sharedManager].persistentContainer.viewContext];
                
                news.pubDate = date;
                news.title   = self.currentTitle;
                news.link    = self.currentLink;
                
                [DataManager.sharedManager saveContext];
                
                [self.news addObject:news];
            }
        }
        
        self.currentLink    = nil;
        self.currentTitle   = nil;
        self.currentPubDate = nil;
        self.currentElement = nil;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    //Update views when parser did end.
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pubDate" ascending:NO];
        
        [self.news sortUsingDescriptors:[[NSArray alloc] initWithObjects:sortDescriptor, nil]]; //Sorting.
        
        [self.newsListCell.tableView reloadData];
    });
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"%@", parseError.localizedDescription);
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    //For offline work.
    News* news = [self.news objectAtIndex:self.selectedCell.row];
    
    news.content = [self.newsContentCell.webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    
    [DataManager.sharedManager saveContext];
}

#pragma mark - My methods
- (void)getRSSData {
    
    NSURL* url = [[NSURL alloc] initWithString:@"http://static.feed.rbc.ru/rbc/internal/rss.rbc.ru/rbc.ru/mainnews.rss"];
    
    NSURLSessionDataTask* URLTask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error) {
            
            [DataManager.sharedManager deleteOldNews]; //Delete old news.
            
            //If online.
            getLocalContent = NO;

            self.news = [NSMutableArray array];
            
            NSXMLParser* parser = [[NSXMLParser alloc] initWithData:data];
            parser.delegate = self;
            
            [parser parse];
            
        } else {
            
            //If offline.
            dispatch_async(dispatch_get_main_queue(), ^{
                
                getLocalContent = YES;
                
                NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
                
                NSEntityDescription* description = [NSEntityDescription entityForName:@"News" inManagedObjectContext:DataManager.sharedManager.persistentContainer.viewContext];
                
                [fetchRequest setEntity:description];
                
                NSSortDescriptor* dateDescription =
                [[NSSortDescriptor alloc] initWithKey:@"pubDate" ascending:NO];
                
                [fetchRequest setSortDescriptors:@[dateDescription]];
                
                NSArray* objects = [DataManager.sharedManager.persistentContainer.viewContext executeFetchRequest:fetchRequest error:nil];
                
                self.news = [[NSMutableArray alloc] initWithArray:objects];                  
                
                [self.newsListCell.tableView reloadData];
            });
        }
    }];
    
    [URLTask resume];
}

@end
