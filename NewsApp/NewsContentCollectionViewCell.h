//
//  NewsContentCollectionViewCell.h
//  NewsApp
//
//  Created by Admin on 02.09.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsContentCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
