//
//  NewsListCollectionViewCell.m
//  NewsApp
//
//  Created by Admin on 02.09.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import "NewsListCollectionViewCell.h"

@implementation NewsListCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

@end
