//
//  DataManager.h
//  NewsApp
//
//  Created by Admin on 03.09.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void) saveContext;
- (NSManagedObject*) haveObjectWithEntityName:(NSString*) entityName predicateDate:(NSDate*) predicateDate;

- (void) deleteOldNews;
- (NSDate*) getCheckDate;

+ (DataManager*) sharedManager;

@end
