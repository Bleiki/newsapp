//
//  MainViewController.h
//  NewsApp
//
//  Created by Admin on 02.09.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface MainViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, NSXMLParserDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSString* currentElement;
@property (strong, nonatomic) NSMutableArray* news;
@property (strong, nonatomic) NSMutableString* currentLink;
@property (strong, nonatomic) NSMutableString* currentTitle;
@property (strong, nonatomic) NSMutableString* currentPubDate;

@end
